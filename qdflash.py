#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# tab: 4

#
# this program is python 2.7 and 3.x compatible
# 3.x is faster !?!
#

import signal
import RPi.GPIO as GPIO
import spidev
import sys, getopt
import os,subprocess
#import json
#import uuid
import time
import mmap
import traceback
import pickle

# LogLevel 0...5 (5 is all)
LogLevel=1

# default OUT
FPGA_RST = 24
#default IN
FPGA_DONE = 25
SPI_CE0   =  8
FPGA_SSOV =  7     # override: switch to out and then drive 1/0 to talk to FPGA

# SPI shoule always be /dev/spidev0.0 (unless wired differently)
spi=None            # handle to spidev
SPI_bus = 0
SPI_device = 0

### Chip Defs
Chip_PhySize=(1<<21)
Chip_Start=0  # 4*0x10000+0x000
Chip_Size=Chip_PhySize - Chip_Start
Chip_End=(Chip_Start+Chip_Size)


Erase_Sector_Size=0x1000
Erase_Block_32_Size=0x8000
Erase_Block_64_Size=0x10000

Write_Chunk_Size=256

MyProgName=sys.argv[0]
argv = sys.argv[1:]

flash_Table= [
    # JPEG ID        name     2^x
    [[239, 64, 21],"W25Q16BV", 21 ]
]

#########



isPython3=(sys.version_info[0]==3)

def get_mono_time():
    if isPython3:
        return time.monotonic()
    else:
        # python 2 does not have monotonic
        return time.time()

#define an exit handler
gblExit = 0
ExitOnCtrl=3
gblInExceptionBlock=0
def signal_handler(signal, frame):
    global gblExit, LogFileHandle
    gblExit+=1
    print('You pressed Ctrl+C {0}/{1} times !'.format(gblExit,ExitOnCtrl))
    if (gblExit >= ExitOnCtrl):
        if gblInExceptionBlock > 0:
            # next time we exit via sys
            gblInExceptionBlock=0
            # exit via Exception
            raise NameError('abort')
        else:
            # if not acting in an try braket
            GPIO.cleanup()
            spi.close()
            print ('.. abort')
            sys.exit(0)
        #fi
    #fi
#enddef

def ice_release_power_down():
    time.sleep(0.001)
    data = [0xAB]
    spi.xfer2(data)
    # wait at least 3 us
    time.sleep(0.003)
#enddef

def ice_rst():
    data = [0xFF, 0xFF, 0xFF, 0x00]
    spi.xfer2(data)
#enddef

def ice_getmfgid():
    #        cmd   nc    nc   0/1   rd 2 more
    data = [0x90, 0x00, 0x00, 0x00, 0, 0 ]
    spi.xfer2(data)
    #print ("mfg:", data)
    return data [4:]
#enddef

def ice_getjdecid():
    # data = [0x9F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
    data = [0x9F, 0xFF, 0xFF, 0xFF]
    spi.xfer2(data)
    #print ("jdec:", data)
    return data [1:]
#enddef

def ice_getstatus1():
    data = [0x05, 0xFF, 0xFF, 0xFF]
    spi.xfer2(data)
    print ("status1:", data)
    return data [-1]
#enddef

def ice_getstatus2():
    data = [0x35, 0xFF, 0xFF, 0xFF]
    spi.xfer2(data)
    print ("status2:", data)
    return data [-1]
#enddef

def ice_uid():
    data = [0x4B, 0x1, 0x2, 0x3, 0x4, 1,2,3,4]
    spi.xfer2(data)
    print ("uid:", data)
    return data [5:]
#enddef

def ice_WriteEnable():
    data = [0x06]
    spi.xfer2(data)
#enddef

def ice_WriteDisable():
    data = [0x04]
    spi.xfer2(data)
#enddef

# primitive read - make for early debugging
def ice_rd():
    data = [0x03, 0, 0, 0, 0, 0, 0,0,0,0,0,0,0,0,0]
    spi.xfer2(data)
    print ("data:", data)
    # return data [-1]
#enddef

#
# read a 4k section with fast read (eventhough we will always use <= 50 MHz)
# (pyspi buffer is allways 4k (even if the kernel is set otherwise), 
#  so length has to be 5 less to leave room for the command)
#
def ice_fast_read_4k(myFile, addr,length):
    myFile.seek(addr)
    # need to use list, bytearray does not return any data
    if isPython3:
        # performance is more consistent in python 3
        data=[0x0b,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr),0] + list(range(length))
    else:
        # better performance in python 2
        data=[0x0b,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr),0] + list(bytearray(length))
    sys.stdout.write ('\rfrd:{0:06X} -> {1:02X} {2:02X} {3:02X} {4:02X}'.format(addr,data[0],data[1],data[2],data[3]))
    # exec cmd and get data back
    spi.xfer2(data)
    # write data
    myFile.write(bytearray(data[5:]))
#enddef

def ice_fast_read_range(addr,length):
    mybuffer=bytearray(length)
    endaddr=addr+length     # stop reading
    chunk=0x1000-0x10       # read in chunks
    # chunk=0x2000-0x10       # read in chunks
    # need to use list, xfer2(bytearray) does not return any data
    # -> hint: range does not work when bypassing xfer2 in testing
    wptr=0
    while (addr < endaddr):
        if (addr+chunk) > endaddr:      # don't read past the end
            chunk = endaddr-addr
        if isPython3:
            # performance is more consistent in python 3 with range
            #data=[0x0b,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr),0] + list(mybuffer[addr:(addr+chunk)])
            data=[0x0b,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr),0] + list(bytearray(chunk))
            # data=[0x0b,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr),0] + list(range(chunk))
        else:
            # better performance in python 2
            data=[0x0b,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr),0] + list(bytearray(chunk))
        #fi
        sys.stdout.write ('\rfrd2:{0:06X} -> {1:02X} {2:02X} {3:02X} {4:02X}'.format(addr,data[0],data[1],data[2],data[3]))
        # exec cmd and get data back
        spi.xfer(data)
        # store data
        mybuffer[wptr:(wptr+chunk)] = data[5:5+chunk]
        addr += chunk
        wptr += chunk
    #wend
    return mybuffer
#enddef

#
# write 1..256 byte of data to addr
#
def ice_program_page(addr, InData):
    if not isinstance(InData,list):
        print ('bad data type for write page')
        return
    length = len(InData)
    if length < 1 or length > 256:
        print ('bad length for write page: {0}' .format(length))
        return
    #fi
    WrData = [0x02,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr)] + InData
    # print (WrData)
    spi.xfer2(WrData)
    # for starters wait just 1 ms and see what the real world comes out at
    ice_busywait(wait_first=0.000,wait_loop=0)

#enddef

# erase 4k sector
def ice_EraseSector(addr):
    EData = [0x20,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr)]
    spi.xfer2(EData)
    ice_busywait(wait_first=0.010)
#enddef

# erase 32k or 64k block
def ice_EraseBlock(addr, size=64):
    if size==64:
        cmd=0xD8
    else:
        cmd=0x52
    EData = [cmd,0xFF & (addr >> 16),0xFF & (addr >> 8),0xFF & (addr)]
    spi.xfer2(EData)
    ice_busywait()
#enddef

# erase the entire chip
def ice_EraseChip():
    EData = [0xC7]
    spi.xfer2(EData)
    # chip erase takes anywhere from 1 - 3 sec
    print ('please be patient, it takes 1-3 sec for the chip erase to finish')
    ice_busywait(timeout=5.000, wait_first=1.000, wait_loop=0.100)
#enddef

#
# wait for the flash to become ready (not bussy)
#
#     timeout:     max total wait (give up)
#     wait_first:  give the device time to settle before querry loop
#     wait_loop:   how fast should the loop be (as in make the host sleep more or have quicker response)
#
STATUS_BUSY_MASK=1
STATUS_WrEn_MASK=2

def ice_busywait(timeout=1.000, wait_first=0.030, wait_loop=0.010):
    start_time = get_mono_time()
    if wait_first > 0:
        time.sleep(wait_first)
    n = 0
    while ((get_mono_time() - start_time) < timeout):
        data = [0x05, 0xFF, 0xFF, 0xFF]
        spi.xfer2(data)
        if (data[-1] & STATUS_BUSY_MASK) == 0:
            #if n >= 5: # wait a min of 5 loops
            break
        if wait_loop > 0:
            time.sleep(wait_loop)
        n += 1
    #wend
    if LogLevel>3:
        print ('waited {0:5.3f} sec for {1:2X} {2} loops'.format(get_mono_time() - start_time,data[-1],n))
#enddef

#def ice_program_range(addr,size):
def ice_program_range(addr,InData):
    size=len(InData)
    addr += Chip_Start
    end_addr=addr+size
    if (end_addr > Chip_End):
        print ('write past end not possible')
        return None
    wno=0
    if LogLevel<=3:
        print ('write range {0:6X} to {1:06X}'.format (addr, end_addr))

    while (addr < end_addr):
        # can only write to the end of the current page (normal size = 256)
        chunk_end = (addr + Write_Chunk_Size) & ~(Write_Chunk_Size-1)
        # only write as many as requested bytes
        if (chunk_end > end_addr):
            chunk_end = end_addr
        wlen=chunk_end-addr
        if LogLevel>3:
            print ('write page @ {0:6X} len = {1:03X}  InData[{2:X}:{3:X}]'.format (addr, wlen, wno, wno+wlen))
        else:
            sys.stdout.write('.')
        ice_WriteEnable()
        ice_program_page(addr, list(InData[wno:wno+wlen]))
        # advance pointers
        wno+=wlen
        addr=chunk_end
    #wend
    print ('\nend write {0:6X}, {1:X} bytes'.format(addr,wno))
#enddef

def ice_erase_range(addr,size):
    addr += Chip_Start
    # find the base address of the first sector
    first =  addr             & ~(Erase_Sector_Size-1)
    # find the base address of the last sector (i.e. the sector with the last byte to write)
    last  = (addr + size - 1) & ~(Erase_Sector_Size-1)
    # last has to be within the chip (not +1)
    if (last >= Chip_End):
        print ('erase past end not possible')
        return None
    # since last is part of the range to delete, the delta is 1 short (i.e. if first==last, there is one sector to delete)
    n = ((last - first) // Erase_Sector_Size) + 1
    print ('need to erase {0} sectors from {1:06X} to {2:06X} @ {3:06X}-{4:06X}'.format(n,first,last,addr,addr+size))
    ptr=first
    while (n > 0):
        ice_WriteEnable()
        if (n >= 16) and (ptr & (Erase_Block_64_Size-1)==0):
            print ('erase block64 {0:06X} - {1:06X}'.format(ptr, ptr | (Erase_Block_64_Size-1)))
            ice_EraseBlock(ptr, size=64)
            n -= 16
            ptr += Erase_Block_64_Size
        elif (n >= 8) and (ptr & (Erase_Block_32_Size-1)==0):
            print ('erase block32 {0:06X} - {1:06X}'.format(ptr, ptr | (Erase_Block_32_Size-1)))
            ice_EraseBlock(ptr, size=32)
            n -= 8
            ptr += Erase_Block_32_Size
        else:
            print ('erase sector  {0:06X} - {1:06X}'.format(ptr, ptr | (Erase_Sector_Size-1)))
            ice_EraseSector(ptr)
            n -= 1
            ptr += Erase_Sector_Size
        #fi
    #wend
    # return the erased range. so that the caller might be able to restore over deleted 
    return (first,ptr)
#enddef

def print_help():
    print ('help??')
#enddef

def get_val(sval):
    sval=sval.strip()
    fac=1
    idx=0

    if (sval[0] == 'B'): # value is in blocks
        fac=64*1024 # Blocks are 64 kByte
        idx=1
    if (sval[0] == 'S'): # value is in sectors
        fac=4*1024 # sector is 4 kByte
        idx=1
    if (sval[-1] == 'h'): # Hex
        val=int(sval[idx: -1], 16)*fac
    else:
        val=int(sval[idx: ], 10)*fac
    return val
#enddef

def parse_vchip(args):
    global Chip_Start, Chip_End, Chip_Size
    try:
        alist=args.split(',')
        vstart= 0
        vsize = 0
        if (len (alist) < 0):
            raise NameError('no arguments')
        vstart=get_val(alist[0])
        if (len (alist) >= 2):
            vsize=get_val(alist[1])

        if (vstart > 0):
            Chip_Start=vstart
            Chip_Size=Chip_PhySize - Chip_Start

        if (vsize > 0):
            Chip_Size=vsize
        # update end of chip
        Chip_End=(Chip_Start+Chip_Size)
        if (Chip_End > Chip_PhySize):
            raise NameError('exeeds physical size')
            
        print ('vchip={0:X}-{1:X} size={2:X}'.format(Chip_Start, Chip_End, Chip_Size))

    except NameError as err:
        print ('vchip: {}'.format(err))
        sys.exit(1)
    except:
        print ('vchip: bad arguments "{}"'.format(args))
        sys.exit(1)

#enddef

PgmData=None
def load_files(args):
    global PgmData
    AddrPtr = 0
    for LoadFile in args:
        try:
            # get the data from file
            fh=open(LoadFile,"rb")
            m = mmap.mmap(fh.fileno(), 0, access=mmap.ACCESS_READ)
            if AddrPtr <= 0:
                PgmData = bytearray(m)
            else:
                PgmData[AddrPtr:] = bytearray(m)
            AddrPtr += len(bytearray(m))
            fh.close()
            m.close()
        except:
            # let me know what happend
            traceback.print_exc()
            sys.exit(1)
        print ('{0:20} loaded. end @ {1:X}'.format(LoadFile, AddrPtr))
        if (AddrPtr > Chip_Size):
            print ('data exeeds vchip size ({0:X} > {1:X})'.format(AddrPtr, Chip_Size))
            sys.exit(2)
    #next
#enddef

def setup_flash_access():
    global spi
    spi = spidev.SpiDev()
    spi.open(SPI_bus, SPI_device)

    GPIO.setmode(GPIO.BCM)
    # a lot of warning that we need to ignore, comment out to see them
    GPIO.setwarnings(False)

    # start at 0.4 Mhz
    # spi.max_speed_hz = 400000
    # or use 1-10 Mhz
    spi.max_speed_hz = 10 * 1000000

    # Default to mode 0, and make sure CS is active low.
    spi.mode=0
    # new version lost this setting
    #spi.cshigh = False
    # setup input's
    GPIO.setup( (FPGA_DONE,FPGA_SSOV) , GPIO.IN)
    # setup reset line: do not reset FPGA when switching IN->OUT
    # make sure SSB is HIGH when reset get's active (we don't want to talk to the fpga)
    GPIO.setup(SPI_CE0,  GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setup(FPGA_RST, GPIO.OUT, initial=GPIO.HIGH)
    # finaly start by activating reset
    GPIO.output(FPGA_RST, GPIO.LOW)
    time.sleep(0.010)    
#enddef

def close_flash_access():
    global spi
    spi.close()
    spi = None
    # as soon as reset in terminated, the FPGA starts to drive the lines (i.e. reads out the flash)
    GPIO.setup( (SPI_CE0,FPGA_RST) , GPIO.IN)
    GPIO.cleanup()
#enddef

def get_uuid_str():
    uid=ice_uid()
    i=0
    while (i<4):
        if i == 0:
            uid_str=''
        else:
            uid_str+='-'
        uid_str+='{0:02X}{1:02X}'.format(uid[i],uid[i+1])
        i+=2
    #wend
    return uid_str
#enddef

def id_chip(fingerprint):
    while (gblExit < 2):
        # clear any junc (resync) and wake spi-flash (should it be sleeping)
        ice_release_power_down()

        # see if we get consistent data
        loop=0
        match=0
        while loop < 10:
            #r = ice_getmfgid()
            #print ('MFG   mfg: {0:X}, id:  {1:X}'.format(r[0],r[1]))
            r = ice_getjdecid()
            print ('JEDEC mfg: {0:X}, typ: {1:X}, cap: {2:X}'.format(r[0],r[1],r[2]))
            loop+=1
            if (loop > 1):
                if len(fingerprint) != 3:
                    fingerprint=r
                else:
                    if fingerprint==r:
                        match+=1
                        if (match >= 3):
                            break
                    else:
                        print ('bad JEDEC id: {0:X}, typ: {1:X}, cap: {2:X}'.format(r[0],r[1],r[2]))
                        raise NameError ('bad JEDEC id')
                    #fi
                #fi
            #fi
        #wend
        time.sleep(0.010)
        break
    #wend
    return fingerprint
#enddef

#
# simple ease (do not look for already empty or fittingly programmed data)
#
def erase_stage(addr, size):
    start_time = get_mono_time()
    if size > 0:
        ice_getstatus1()
        ice_WriteEnable()       # not too usefull, needs to be done before every call of any erase cmd
        ice_getstatus1()
        # now we can program...
        ice_erase_range(addr,size)
        ice_WriteDisable()      # should alredy be off, since every command reset's teh write enable bit
        ice_getstatus1()
    #fi
    print('\n\n ... erase : {0:5.4f} sec'. format(get_mono_time() - start_time))
#enddef

def pgm_stage(addr, PgmData):
        start_time = get_mono_time()
        # get started by enabling write
        ice_getstatus1()
        ice_WriteEnable()       # see before, silly - but why not
        ice_getstatus1()
        # now we can program...
        ice_program_range(addr,PgmData)
        # end
        ice_WriteDisable()
        # ice_getstatus1()
        print('\n\n ... write : {0:5.4f} sec'. format(get_mono_time() - start_time))
#enddef

def read_stage(addr, size, FName):
    start_time = get_mono_time()
    if size > 0:
        try:
            myFile = open(FName, "wb")
            # verify the write by reading, but only the section of interest
            data=ice_fast_read_range(addr,size)
            myFile.write(data)
        except:
            # let me know what happend
            traceback.print_exc()
    #fi
    print('\n\n ... read : {0:5.4f} sec'. format(get_mono_time() - start_time))
#enddef


############################################
#        MAIN (testing stage)
############################################
doPreTest=0
if (doPreTest):
    start_time=get_mono_time()
    #by=bytearray(4*1024)
    #myFile.write(by)
    #myFile.close()
    #data=ice_fast_read_range(0,2*1024*1024)
    #ice_program_range (Chip_End - Chip_Start - 1234,1234)  
    ice_program_range (0x0,bytearray(0x444))
    ice_erase_range(0x0800,130*1024+1)
    print('\n\n test ended : {0:5.4f} sec'. format(get_mono_time() - start_time))
    
    sys.exit(0)
#end doPreTest
# get spi handle

############################################
#         MAIN (get opts)
############################################
rd_all=False
rd_chip=False

try:
    opts, args = getopt.getopt(argv,"v:",["vchip=", "rd-all", "rd-chip"])
except getopt.GetoptError:
    print_help()
    sys.exit(2)

# print (opts, args)

for opt, arg in opts:
    if len(arg) > 1 and arg[0] == '=':
        arg=arg[1:]
    if opt == '-h' or opt == '--help':
        print_help()
        sys.exit()
    elif opt == '--rd-all':
        rd_all=True
    elif opt == '--rd-chip':
        rd_chip=True
    elif opt in ("-v", "--vchip"):
        parse_vchip(arg)
    #fi
#next

if len(args) < 1:
    print ('no data to flash?')
    sys.exit(1)

load_files(args)
print ('FLASH {0} bytes begin {1}'.format(len(PgmData), PgmData[0:16]))


# first setup a terminate handler
signal.signal(signal.SIGINT, signal_handler)
chipid=[]
try:
    gblInExceptionBlock=1

    # print ('READ  DATA {} / {}')
    setup_flash_access()
    chipid=id_chip(chipid)
    chipName='unknown'
    if chipid == flash_Table[0][0]:
        chipName=flash_Table[0][1]
        if flash_Table[0][2] != 21:
            print ('FLASH length not 2MByte ({0:X}h)'.format(1 << flash_Table[0][2]))
            raise NameError('bad flash chip')
        #fi
    #fi
    print ('chipid=',chipid,chipName)
    erase_stage(0,len(PgmData))
    pgm_stage(0,PgmData)
    if (rd_chip):
        read_stage(0, Chip_PhySize, "./readback_flash_all.bin")        
    elif (rd_all):
        read_stage(Chip_Start, Chip_Size, "./readback_flash.bin")
    else:
        read_stage(Chip_Start, len(PgmData), "./readback_flash_smart.bin")
except NameError as err:
    print ('exit via: {0}'.format(err))
except:
    # let me know what happend
    traceback.print_exc()

gblInExceptionBlock=0
print ('cleanup')

GPIO.output(FPGA_RST, GPIO.HIGH)
GPIO.cleanup()
spi.close()

print ('.. done')
sys.exit(0)

############################################
#         END MAIN
############################################
