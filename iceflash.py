#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# tab: 4

# Copyright (c) 2010-2020, Sven G. Conrad <svengro@gmail.com>
# All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


#
# Programming an olimex iCE40HX attached to an raspberry pi
# using the SPI port + gpio24 & gpio25
#

import signal
import RPi.GPIO as GPIO
import spidev
import sys, getopt
import os,subprocess
#import json
#import uuid
import time
import mmap

# default OUT
FPGA_RST = 24
#default IN
FPGA_DONE = 25
FPGA_SSOV = 7       # override: switch to out and then drive 1/0 to talk to FPGA

# SPI shoule always be /dev/spidev0.0 (unless wired differently)
SPI_bus = 0
SPI_device = 0

############################################
#                 Main Vars
############################################

MyProgName=sys.argv[0]
argv = sys.argv[1:]
MirrorFileName=''
MirrorFilePath1='/run/temp/'
MirrorFilePath2='/tmp/'
MirrorData=None

ReadFileName=''
OutFileName =''
cmd=0
WriteMirror=2
CopyMirror=2
Refresh_Mirror=False

EmptySector=bytearray(256)

############################################
#                 Flash Vars
############################################

flash_JPEG_ID=0xEF4015
flash_Name="W25Q16BV"
flash_Size=1<<21
flash_UniqueId="D965-B4C2"

flash_Table= [
    # JPEG ID   name     2^x
    [0xEF4015,"W25Q16BV", 21 ]
]

############################################
#                 Sub
############################################

#define an exit handler
gblExit = 0
ExitOnCtrl=3
gblInExceptionBlock=0
def signal_handler(signal, frame):
    global gblExit, LogFileHandle
    gblExit+=1
    print('You pressed Ctrl+C {0}/{1} times !'.format(gblExit,ExitOnCtrl))
    if (gblExit >= ExitOnCtrl):
        if gblInExceptionBlock > 0:
            # next time we exit via sys
            gblInExceptionBlock=0
            # exit via Exception
            raise NameError('abort')
        else:
            # if not acting in an try braket
            GPIO.cleanup()
            spi.close()
            print ('.. abort')
            sys.exit(0)
        #fi
    #fi
#enddef

isPython3=(sys.version_info[0]==3)

def get_mono_time():
    if isPython3:
        return time.monotonic()
    else:
        # python 2 does not have monotonic
        return time.time()
#enddef


def get_mirror_file():
    global MirrorFileName,MirrorData,MirrorFilePath1,MirrorFilePath2
    global flash_Name, flash_UniqueId
    start = get_mono_time()
    if len (MirrorFileName) < 1:
        FName="Flash_{0}-{1}.bin".format(flash_Name,flash_UniqueId)
        MirrorFileName=os.path.join(MirrorFilePath1,FName)
    #fi
    if not os.path.isfile(MirrorFileName):
        print ("mirror files does not exist",MirrorFileName)
        return
    #fi
    #print ("load ",MirrorFileName)
    fh=open(MirrorFileName,"rb")
    m = mmap.mmap(fh.fileno(), 0, access=mmap.ACCESS_READ)
    MirrorData = bytearray(m)
    fh.close()
    m.close()
    print ('read {1} in {0:5.3} sec'.format(get_mono_time() - start, MirrorFileName))
#enddef

def get_flash_usage():
    global MirrorData, TopSectorAddr, LastUsedByte
    # scan for usage
    start = get_mono_time()
    n = len(MirrorData)
    # scan Mirror sector by sector (byte by byte takes for ever)
    while (n > 256):
        n-=256
        if not MirrorData[n:n+256] == EmptySector:
            break
    #wend
    n += 256
    TopSectorAddr=n
    while (n > 1):
        n -= 1
        if (MirrorData[n] != 0xFF):
            break
    #wend
    LastUsedByte = n
    print ('usage in {0:5.3} sec'.format(get_mono_time() - start))
#enddef

#
# Create an bytearray filled with some value (filler)
#
def Create_FilledByteArray(size, filler=0xFF):
    data = bytearray(size)
    data[0] = filler
    n = 1
    while (n < size):
        if (2*n > size):
            # size is not power of 2, so fill only what's left
            # print ('n = {} size={}'.format(n,size))
            data[n:] = data[0:size-n]
            # we know it's done
            break
        else:
            data[n:] = data[0:n]
        n = 2*n
    #wend
    # print (data)
    return data
#enddef

def print_help():
    print (MyProgName + ' args\n'
        '\t-h  |  --help                  print this help\n'
        '\t-m  |  --mirror-file <file>    change mirror file name\n'
        '\t-f  |  --refresh-mirror        read mirror file\n'
        '\t    |  --write-mirror          write used portion of mirror image to file\n'
        '\t    |  --copy-mirror           write all of mirror image to file\n'
        )
#enddef

############################################
#                 MAIN
############################################

# init some early vars
#
start = get_mono_time()
xx=Create_FilledByteArray(17)
print ('len={0} => {1}'.format(len(xx),xx))
EmptySector=Create_FilledByteArray(0x100)
NewFlashImage=Create_FilledByteArray(1<<21)
print ('Create_FFByteArray {0:5.3} sec'.format(get_mono_time() - start, MirrorFileName))
sys.exit(0)

try:
    opts, args = getopt.getopt(argv,"hm:fr:",["help","mirror-file=","refresh-mirror","read","write-mirror=","copy-mirror="])
except getopt.GetoptError:
    print_help()
    sys.exit(2)

print (opts, args)

for opt, arg in opts:
    if opt == '-h' or opt == '--help':
        print_help()
        sys.exit()
    elif opt in ("-m", "--mirror-file"):
        MirrorFileName = arg
    elif opt in ("-f", "--refresh-mirror"):
        Refresh_Mirror=True
    elif opt in ("-r", "--read"):
        ReadFileName = arg
    elif opt in ("-u", "--write-mirror"):
        OutFileName = arg
        cmd=WriteMirror
    elif opt in ("-U", "--copy-mirror"):
        OutFileName = arg
        cmd=CopyMirror
    #fi
#next

print ('MirrorFileName is ', MirrorFileName)
#print 'Output file is "', outputfile
get_mirror_file()
get_flash_usage()
if (cmd == WriteMirror):
    fn=open(OutFileName,"wb")
    fn.write(MirrorData[0:LastUsedByte+1])
    fn.close()
elif (cmd == CopyMirror):
    fn=open(OutFileName,"wb")
    fn.write(MirrorData)
    fn.close()
#fi
#
print ('first free sector = {0:X}h, last byte {1:X}h, of {2:X}h'.format(TopSectorAddr, LastUsedByte, len(MirrorData)))
sys.exit(0)

############################################
#                 END
############################################
